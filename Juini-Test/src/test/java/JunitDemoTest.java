import org.junit.jupiter.api.*;
import org.junit.jupiter.api.DisplayName;

import java.util.Arrays;
import java.util.List;


@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class JunitDemoTest {

    JunitDemo junitDemo;

    /**
     * When we can use before and after each ,
     * 1. We can set up one time before all tests
     * Get DB connection , connect to remote server
     * One time clean up after all tests
     * Release DB connections , disconnected from remote servers
     * Before All and after all
     */


    @BeforeEach
    void setupBeforeEach() {
        junitDemo = new JunitDemo();
        System.out.println("Initialise the objets before start");
    }

    @BeforeAll
    @DisplayName("Initialize the all setup Before All ")
    static void setupBeforeAll() {
        System.out.println("setup the objets before start");

    }

    static @AfterAll
    void cleanAfterAll() {
        System.out.println("Clean the objets after all");

    }


    @Test
    @DisplayName("The testEqualAndNotEquals")
    void testEqualAndNotEquals() {

        int expected = 8;
        int actual = junitDemo.add(4, 4);
        Assertions.assertEquals(expected, actual, "4+4 must be 8");
        int unExpActual = junitDemo.add(4, 4);
        int unexpected = 4;
        Assertions.assertNotEquals(unexpected, unExpActual, "4+6 must be 8");

    }

    @Test
    void test_Null_And_Not_Null_Underscore() {

        String str1 = null;
        String str2 = "Raj";
        Assertions.assertNull(junitDemo.checkNull(str1), "object should null ");
        Assertions.assertNotNull(junitDemo.checkNull(str2), "object should null ");

    }

    @Test
    void testNullAndNotNull() {

        String str1 = null;
        String str2 = "Raj";
        Assertions.assertNull(junitDemo.checkNull(str1), "object should null ");
        Assertions.assertNotNull(junitDemo.checkNull(str2), "object should null ");

    }

    @AfterEach
    void tearDownAfterEach() {
        System.out.println("clear the all objets");

    }


    /**
     * Assertion Test same or not same
     */

    @Test
    @DisplayName("Test Same or not ")
    void testSameOrNotSame() {

        String str = "RajY";

        Assertions.assertSame(junitDemo.getAcademy(), junitDemo.getAcademy(), "Result same");
        Assertions.assertNotSame(str, junitDemo.getAcademy(), "Result should not be same");
    }

    @Test
    @DisplayName("Is true method or not ")
    void isTrueMethodOrNot() {

        int grade1 = 10;
        int grade2 = 5;

        Assertions.assertTrue(junitDemo.isGreater(grade1, grade2), "This should return true");

        Assertions.assertFalse(junitDemo.isGreater(grade2, grade1), "This should not same");
    }

    @Test
    @DisplayName("Array list ")
    void getAcademyListEqualsTest() {
        String[] stringArray = {"A", "B", "C"};
        Assertions.assertArrayEquals(stringArray, junitDemo.getFirstThreeletterAplhbet(), "Array iterable ");

    }

    @Test
    @DisplayName("Iterable Array list")
    void getAcademyIterableList() {
        List<String> theList = Arrays.asList("RAj", "2", "QWW");
        Assertions.assertIterableEquals(theList, junitDemo.getAcademyList(), "Iterable list are equals");

    }

    @Test
    @DisplayName("getAcademyLineMatchList Array list")
    void getAcademyLineMatchList() {
        List<String> theList = Arrays.asList("RAj", "2", "QWW");
        Assertions.assertLinesMatch(theList, junitDemo.getAcademyList(), "Iterable list are equals");

    }


}

